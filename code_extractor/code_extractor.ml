let get_lines filename =
  let lines = ref [] in
  let chan = open_in filename in
  try
    while true; do
      lines := input_line chan :: !lines
    done; !lines
  with End_of_file ->
    close_in chan;
    List.rev !lines ;;

let rec until_pred f = function
  | [] -> []
  | hd :: tl -> (
      if not (f hd)
      then hd :: (until_pred f tl)
      else []
    )

let () =
  let path = Sys.argv.(1) in
  let lines = get_lines path in
  let aux x = x = "open Property" in
  let prelines = until_pred aux lines in
  List.iter (Format.printf "%s\n") prelines ;
  ()
