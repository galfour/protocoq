(* Code *)  
let f x = 2 * x

let g x = -x

(* Everything after this is ignored by `coq_of_ocaml` through `code_extractor.exe` *)
open Property

let f_properties = function_properties ~function_name:"f" [
    property ~name:"positivity" "forall x , x > 0 -> f x > 0" ;
    property ~name:"2_4" "f 2 = 4" ;
  ]

let g_properties = function_properties ~function_name:"g" [
    property ~name:"swap_sign" "forall x , x >= 0 <-> g x <= 0" ;
    property ~name:"1_m1" "g 1 = -1" ;
  ]

let properties : file_properties = [
  f_properties ;
  g_properties ;
]
