# Notes

## Intro

The goal of this is to be able to drop coq specifications for small functions in an `OCaml` file, and have them checked by `coq` automatically. The `coq` checking should be resilient to renamings and other modifications, so that the whole thing is maintainable.
This is to traditional formal verification what unit tests are to end-to-end tests.

## Installation

This assumes `dune`, `opam` and `coq-of-ocaml` are installed. For the later, check out [this page](https://clarus.github.io/coq-of-ocaml/docs/install.html).

## Usage

Go into `example/my_functions.ml`. You can define more functions and add more properties in `properties`. 
No code to be evaluated by `coq` should be put after `open Property`.
To check if the properties are ok, run `dune build @coq_check` twice (twice, because of a weird bug)

The generated coq file is `full.v`. You can display it with `cat _build/default/example/full.v`.

## Details

- `dune build code_extractor/code_extractor.exe` builds the code extractor. A dumb bin that syntactically cuts everything after (including) the line `open Property`
- `dune build example/code_functions.ml` creates an OCaml file containing only the functions, without the properties, through `code_extractor.exe`
- `dune build example/Code_functions.v` calls `coq-of-ocaml` on `code_functions.ml`
- `dune build example/properties.v` generates the coq theorems from the properties described in `My_functions` using `Property.Builder`
- `dune build example/full.v` concatenates the code generated in `Code_functions.v` and `properties.v`
- `dune build @coq_check` compiles `full.v`, which checks all the theorems contained in it. Should be run twice.

## TODO

### Maintenance

- Tests
  - Fail gracefully
    - Ask `dune` Gods
  - Capture which properties failed
    - Ask G Claret
  - Have paths not hard-coded in the `dune` file
    - Ask `dune` Gods
  - Given this, write the tests

- Make opam packages for everything
  - List: `code_extractor`, `property_builder`, `example`
  - Once this is done, replace `../code_extractor/code_extractor.exe` in `./example/dune` by however `dune` calls installed bins
- Add comments in generated code

### Bugs

- Investigate why one should run `dune build @coq_check` twice after having split stuff in sub folders

### Features

- Have paths not hard-coded in the `dune` file
  - Ask `dune` Gods about it
- Make it work across multiple files
  - Ask G Claret how to manage dependencies with `coq_of_ocaml`
- Find a way to separate code from properties less brittle than looking for `Open Property`
- Find a way to promote `full.v`
- Implement a better crush tactic

- Test the whole thing

- Add some ppx
  - To create properties quickly, in particular, without having to provide `function_name`