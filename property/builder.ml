open Base

let tactic_to_coq : tactic -> string = function
  | Lia -> "lia"
  | Crush -> "crush"
  | Auto -> "auto"

let run : file_properties -> unit = fun fun_properties ->
  Format.printf "\n\n" ;
  Format.printf "Require Import Coq.micromega.Lia .\n\n" ;
  Format.printf "Ltac crush := auto ; lia .\n\n" ;
  let aux_function { function_name ; properties } =
    let aux_property { name ; statement ; tactic } =
      Format.printf "Theorem %s_%s : %s .\n unfold %s . %s . \nQed .\n\n"
        function_name name statement function_name (tactic_to_coq tactic)
    in
    List.iter aux_property properties
  in
  List.iter aux_function fun_properties
