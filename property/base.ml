type tactic =
  | Auto
  | Crush
  | Lia

type property = {
  name : string ;
  statement : string ;
  tactic : tactic ;
}

type fun_properties = {
  function_name : string ;
  properties : property list ;
}

type file_properties = fun_properties list

let property ~name ?(tactic = Crush)  statement = { name ; statement ; tactic }

let function_properties ~function_name properties = { function_name ; properties }
